module.exports = class Util {
	static splitIntoPages(arr, elementsPerPage) {
		return new Array(Math.ceil(arr.length / elementsPerPage)).fill().map((_, i) => arr.slice(i * elementsPerPage, i * elementsPerPage + elementsPerPage));
	}

	static parseProps(props) {
		if (!props) {
			return;
		}

		try {
			return JSON.parse(props);
		} catch (e) {
			return;
		}
	}
}