exports.run = async (client, packet) => {
    if (!['MESSAGE_REACTION_ADD'].includes(packet.t)) {
		return;
	}
    
    const channel = client.channels.cache.get(packet.d.channel_id);

    if (channel.messages.cache.get(packet.d.message_id)) {
        return;
	}
	
    const message = await channel.messages.fetch(packet.d.message_id);

    const emoji = packet.d.emoji.id || packet.d.emoji.name;
    const reaction = message.reactions.cache.get(emoji);

    const user = client.users.cache.get(packet.d.user_id) || await client.users.fetch(packet.d.user_id);

    if (!user) {
        return;
	}
    
    if (reaction) {
        reaction.users.cache.set(packet.d.user_id, user);
	}
    
    client.emit('messageReactionAdd', reaction, user);
}