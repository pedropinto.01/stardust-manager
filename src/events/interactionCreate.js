const {Token} = require('stardust-client');

exports.run = async (client, interaction) => {
	if (!interaction.isMessageComponent()) {
		return;
	}

	const messageId = interaction.message.id;

	if (interaction.user.bot) {
		return;
	}

	const userId = interaction.user.id;
	const drop = client.drops.get(messageId);

	if (!drop) {
		return;
	}

	if (interaction.customId !== 'GET_DROP') {
		return;
	}
	
	if (drop.users.includes(userId)) {
		return;
	}

	const options = {
		templateId: drop.templateId,
		amount: "1"
	};

	if (drop.props) {
		options.props = drop.props;
	}

	await Token.mint({
		uniqueId: userId,
		tokenObjects: [options]
	});

	interaction.reply({content: 'You received the drop!', ephemeral: true});

	drop.users.push(userId);

	client.drops.set(messageId, drop);

	if (drop.users.length >= drop.amount) {
		client.drops.delete(messageId);

		interaction.message.delete().catch(console.error);
	}
}