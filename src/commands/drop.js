const {Command} = require('dolphin-discord');
const {MessageEmbed, MessageActionRow, MessageButton} = require('discord.js');
const {Template} = require('stardust-client');
const util = require('../util');

module.exports = class Drop extends Command {
	constructor() {
		super({
			name: 'drop',
			description: 'Drops a certain amount of tokens',
			options: [
				{
					type: 'NUMBER',
					name: 'template_id',
					description: 'Template Id',
					required: true
				},
				{
					type: 'NUMBER',
					name: 'amount',
					description: 'Amount',
					required: true
				},
				{
					type: 'STRING',
					name: 'props',
					description: 'Additional properties if NFT'
				}
			],
			isSlashCommand: true
		});
	}

	async run() {
		const templateId = this.interaction.options.getString('template_id');
		const amount = this.interaction.options.getNumber('amount');
		const props = this.interaction.options.getString('props');

		const parsedProps = util.parseProps(props);

		if (props && !parsedProps) {
			return this.say({
				content: 'Your additional properties seem to be wrong formatted.',
				ephemeral: true
			});
		}

		const template = await Template.get(templateId);

		if (!template) {
			return this.say({
				content: `Template with id \`${templateId}\` not found.`,
				ephemeral: true
			});
		}

		const channel = this.client.channels.cache.get(this.client.config.channelId);

		if (!channel) {
			return;
		}

		const embed = new MessageEmbed()
			.setAuthor(`NEW DROP - ${template.name}`)
			.setDescription('Click the emoji below to receive the drop.')
			.setImage(template.props.mutable.image)
			.setTimestamp();

		const component = new MessageActionRow().addComponents(
			new MessageButton()
				.setLabel('Get Drop')
				.setCustomId('GET_DROP')
				.setStyle('PRIMARY')
		);

		const message = await channel.send({embeds: [embed], components: [component]});

		this.client.drops.set(message.id, {
			amount,
			templateId,
			props: template.type === 'NFT' ? props : null,
			users: []
		});

		this.say({
			content: `Posted the drop at ${channel}`,
			ephemeral: true
		});
	}
}