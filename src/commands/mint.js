const {Command} = require('dolphin-discord');
const {Token, Template} = require('stardust-client');

module.exports = class Mint extends Command {
	constructor() {
		super({
			name: 'mint',
			description: 'Mint a token to a user',
			options: [
				{
					type: 'NUMBER',
					name: 'template_id',
					description: 'Template Id',
					required: true
				},
				{
					type: 'STRING',
					name: 'unique_id',
					description: 'Unique Id',
					required: true
				},
				{
					type: 'NUMBER',
					name: 'amount',
					description: 'Amount'
				}
			],
			isSlashCommand: true
		});
	}

	async run() {
		const templateId = this.interaction.options.getNumber('template_id');
		const uniqueId = this.interaction.options.getString('unique_id');
		const amount = this.interaction.options.getNumber('amount');

		const template = await Template.get(templateId);

		if (!template) {
			return this.say({
				content: `Template with id \`${templateId}\` not found.`,
				ephemeral: true
			});
		}

		await Token.mint({
			uniqueId,
			tokenObjects: [
				{
					templateId,
					amount: "1" ?? amount.toString()
				}
			]
		});

		this.say({
			content: `Successfully minted **${template.name}** to \`${uniqueId}\``,
			ephemeral: true
		});
	}
}