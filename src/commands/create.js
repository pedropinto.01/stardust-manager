const {Command} = require('dolphin-discord');
const {MessageEmbed} = require('discord.js');
const {Template} = require('stardust-client');

const util = require('../util');

module.exports = class Create extends Command {
	constructor() {
		super({
			name: 'create',
			description: 'Create a template',
			options: [
				{
					type: 'STRING',
					name: 'name',
					description: 'Name',
					required: true
				},
				{
					type: 'STRING',
					name: 'image',
					description: 'Image',
					required: true
				},
				{
					type: 'STRING',
					name: 'type',
					description: 'Type',
					choices: [
						{name: 'NFT', value: 'NFT'},
						{name: 'FT', value: 'FT'}
					],
					required: true
				},
				{
					type: 'NUMBER',
					name: 'cap',
					description: 'Template Cap. 0 for unlimited',
					required: true
				},
				{
					type: 'STRING',
					name: 'props',
					description: 'Additional properties'
				}
			],
			isSlashCommand: true
		});
	}

	async run() {
		const name = this.interaction.options.getString('name');
		const image = this.interaction.options.getString('image');
		const type = this.interaction.options.getString('type');
		const cap = this.interaction.options.getNumber('cap');
		const props = this.interaction.options.getString('props');

		const parsedProps = util.parseProps(props);

		if (props && !parsedProps) {
			return this.say({
				content: 'Your additional properties seem to be wrong formatted.',
				ephemeral: true
			});
		}

		const response = await Template.create({
			name,
			cap: cap.toString(),
			type,
			props: {
				immutable: {},
				mutable: {
					image,
					...(parsedProps ?? {})
				},
				$mutable: {}
			}
		});

		const embed = new MessageEmbed()
			.setAuthor('Template Created')
			.setImage(image)
			.setColor(this.client.dolphinOptions.mainColor)
			.setTimestamp()
			.setDescription(`ID: **${response.templateId}**`)
			.addField('Name', name, true)
			.addField('Cap', cap.toString(), true)
			.addField('Type', type, true);

		if (parsedProps) {
			const keys = Object.keys(parsedProps);

			for (const key of keys) {
				embed.addField(key, parsedProps[key].toString(), true);
			}
		}

		this.say({
			embeds: [embed],
			ephemeral: true
		});
	}
}