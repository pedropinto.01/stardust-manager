const {Command} = require('dolphin-discord');
const {MessageEmbed} = require('discord.js');
const {Template} = require('stardust-client');
const {Pagination} = require('discord-interface');

const util = require('../util');

module.exports = class View extends Command {
	constructor() {
		super({
			name: 'view',
			description: 'View templates',
			options: [
				{
					type: 'NUMBER',
					name: 'start',
					description: 'Starting index',
					required: true
				},
				{
					type: 'NUMBER',
					name: 'limit',
					description: 'Limit',
					required: true
				},
				{
					type: 'STRING',
					name: 'filter',
					description: 'Filter'
				}
			],
			isSlashCommand: true
		});
	}

	async run() {
		const start = this.interaction.options.getNumber('start');
		const limit = this.interaction.options.getNumber('limit');
		const filter = this.interaction.options.getString('filter');

		if (limit < 1 || limit > 100) {
			return this.say({
				content: 'Invalid limit. Please keep the limit between 1 to 100',
				ephemeral: true
			});
		}

		const options = {
			start,
			limit
		};

		if (filter) {
			options.filter = filter;
		}

		const templates = await Template.getAll(options).catch(console.error);

		if (!templates) {
			return this.say({
				content: 'Unable to fetch the templates. Please check your parameters or try again later.',
				ephemeral: true
			});
		}

		const pages = util.splitIntoPages(templates, 10);

		const getPageMessageOptions = (currentTemplates, pageNumber) => {
			const description = currentTemplates.map(template => `\`${template.id}\` - ${template.name}`).join('\n\n');

			const embed = new MessageEmbed()
				.setDescription(description)
				.setColor(this.client.dolphinOptions.mainColor)
				.setFooter(`Page ${pageNumber}/${pages.length}`)
				.setTimestamp();

			return {embeds: [embed]};
		};

		Pagination.create({
			interaction: this.interaction,
			userId: this.interaction.user.id,
			items: pages,
			messageOptions: {},
			getPageMessageOptions,
			searchMessageOptions: {content: `Choose a number between **1** and **${pages.length}**`}
		});
	}
}