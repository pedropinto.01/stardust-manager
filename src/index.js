const {Client} = require('dolphin-discord');
const {Collection} = require('discord.js');

require('dotenv').config();

process.on('uncaughtException', console.error);

const client = new Client(require('../dolphin_config.json'));

client.drops = new Collection();
client.config = require('../config.json');

client.login(process.env.TOKEN);