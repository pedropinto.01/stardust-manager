# Setup

- Rename `.env.example` to `.env` and add your confidential information
- Edit `config.json` to your desired drop channel
- Execute `npm install`
- Execute `npm run start`
- Your bot is up

# WARNING

This is a bot example, currently the drops DO NOT USE any type of database they are stored locally during the drop and discarded once it ends. If the bot happens to somehow disconnects or throw an error the current drops will be dismissed it is advised the use of a database.